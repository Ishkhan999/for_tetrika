import re
from pprint import pprint
import requests
from bs4 import BeautifulSoup
import sys


def first_char_is_cyrillic(text):
    return bool(re.search('[а-яА-Я]', text[0]))


def contains_english(text):
    return bool(re.search('[a-zA-Z]', text[0]))


def store_animals_in_txt_file():
    """
    This method continuously parse every wikipedia web page
    and store every animal name existing in that page (which starts with russian letter)
    in txt file named 'animal.txt'
    """
    wikipedia_url = 'https://ru.wikipedia.org/wiki/Категория:Животные_по_алфавиту'
    requested_page = requests.get(wikipedia_url).text
    while True:
        soup = BeautifulSoup(requested_page, 'lxml')
        animals = soup.find('div', class_='mw-category-group').find_all('a')
        for animal in animals:
            if first_char_is_cyrillic(animal.text):
                with open("Animals/animals.txt", "a") as file_object:
                    file_object.write(animal.text)
                    file_object.write("\n")
                print(animal.text)
            if contains_english(animal.text):
                sys.exit()

        urls = soup.find('div', id='mw-pages').find_all('a')
        for elem in urls:
            if elem.text == 'Следующая страница':
                wikipedia_url = 'https://ru.wikipedia.org/' + elem.get('href')
                requested_page = requests.get(wikipedia_url).text


def read_all_lines_of_animal_file():
    """
    This method read each line from 'animal.txt' file,
    count each letter of the alphabet and print the result
    """
    container = {}
    with open('Animals/animals.txt') as lines:
        for line in lines:
            for elem in line[0]:
                if elem in container:
                    container[elem] += 1
                else:
                    container[elem] = 1
        pprint("Number of animals for each letter of the alphabet : " + str(container))


if __name__ == '__main__':
    # store_animals_in_txt_file()
    read_all_lines_of_animal_file()
