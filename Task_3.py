from pprint import pprint


def appearance(intervals):
    events = []

    for elem in intervals:
        event = intervals[elem]

        for j in range(len(event)):
            events.append((event[j], 1 - 2 * (j % 2)))
    pprint(events)

    events.sort()
    passed_time = 0
    counter = 0
    start = -1

    for elem in events:
        counter += elem[1]
        if counter == 3:
            start = elem[0]
        if counter == 2 and start > 0:
            passed_time += elem[0] - start
            start = -1
    print("In seconds: ", passed_time)


tests = [
    {'data': {'lesson': [1594663200, 1594666800],
              'pupil': [1594663340, 1594663389, 1594663390, 1594663395, 1594663396, 1594666472],
              'tutor': [1594663290, 1594663430, 1594663443, 1594666473]},
     'answer': 3117
     },
    {'data': {'lesson': [1594692000, 1594695600],
              'pupil': [1594692033, 1594696347],
              'tutor': [1594692017, 1594692066, 1594692068, 1594696341]},
     'answer': 3565
     }
]

if __name__ == '__main__':
    for i, test in enumerate(tests):
        appearance(test["data"])
        print("_____")
