def task(array):
    first_number = array[0]  # this is the first char in given array
    non_repeating_index = 0
    for ind, value in enumerate(array):
        if value == first_number:
            non_repeating_index += 1
            print("Number 1", value, "Index", ind)
        else:
            print("Number 0", value, "Index", ind)

    return "OUT: " + str(non_repeating_index)


print(task("111111111111111111111111100000000"))
